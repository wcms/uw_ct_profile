/**
 * @file
 * Javascript for editing uw_ct_event.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.uw_ct_profile_edit = {
    attach: function (context, drupalSettings) {

      // Once an ajax request (contact sync) has been completed,
      // ensure that the required is still on the summary.
      $(document).ajaxComplete(function (event, request, settings) {

        // Ensure that the blank summary checkbox and the details/summary
        // exists on the page.
        if (
          $('input[id^="edit-field-uw-blank-summary-value"]').length &&
          $('details[id^="edit-group-summary"] summary').length
        ) {

          // If the intentionally leave summary is not checked and the
          // summary in the details does not have the form-required
          // class, then add form-required to the group and the summary.
          if (
            !$('input[id^="edit-field-uw-blank-summary-value"]').is(':checked') &&
            !$('details[id^="edit-group-summary"] summary').hasClass('form-required')
          ) {

            $('details[id^="edit-group-summary"] summary').addClass('form-required');
            $('label[for^="edit-field-uw-profile-summary-"]').addClass('form-required');
          }
        }
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
